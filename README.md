power_django_template
=======================

Project template for django 1.7 with Admin Tools and PowerAdmin

# How to install

django-admin.py startproject --template=https://bitbucket.org/leonardocsantoss/power_django_template/get/master.zip meuprojeto

cd meuprojeto

pip install -r requirements.txt