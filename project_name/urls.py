#! /usr/bin/env python2.7
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.views import serve
from django.views.generic.base import RedirectView
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/admin/')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        # static
        url(r'^%s(?P<path>.*)$' % settings.STATIC_URL.lstrip('/'), serve,
            {'show_indexes': True, 'insecure': False}),
        # media
        url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL.lstrip('/'), serve,
            {'show_indexes': True, 'insecure': False}),
    )