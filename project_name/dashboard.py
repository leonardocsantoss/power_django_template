# -*- encoding: utf-8 -*-

"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'campus.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'campus.dashboard.CustomAppIndexDashboard'

Para LIMPAR uma base após atualização do dashboard:

python manage dbshell
mysql> truncate admin_tools_dashboard_preferences;

"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name

from datetime import datetime, date

class CustomIndexDashboard(Dashboard):
    title = ''
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        request = context['request']

        self.children += [
            modules.ModelList(
                u'Adminstração',
                models=('django.contrib.*',),
            ),
            modules.RecentActions(
                _('Recent Actions'),
                limit=10
            )
        ]
        
class CustomAppIndexDashboard(AppIndexDashboard):

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=10
            )
        ]        