# -*- encoding: utf-8 -*-
from default import *

# local settings
DEBUG = True
TEMPLATE_DEBUG = DEBUG


DATABASES = {
    'default': {
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.{{ db_engine }}',
        'NAME': '',
        # The rest is not used with sqlite3:
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}